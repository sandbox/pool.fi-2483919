<?php
/**
 * @file
 * Content html for a modal.
 */
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="<?php print $hidden;?>">&times;</span></button>
  <?php if (isset($title)):?>
    <h4 class="modal-title" id="formTitle">
      <?php print $title;?>
    </h4>
  <?php endif;?>
</div>
<?php if (isset($content) || isset($messages)):?>
  <div class="modal-body">
    <?php print $messages;?>
    <?php print $content;?>
  </div>
<?php endif;?>
<?php if (isset($footer)):?>
  <div class="modal-footer">
    <?php print $footer;?>
  </div>
<?php endif;?>
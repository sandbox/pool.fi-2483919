<?php
/**
 * @file
 * Wrapping html for a modal.
 */
?>
<div id="<?php print $wrapper_id;?>">
  <div class="modal fade in bootstrap-modal" tabindex="-1" role="dialog" aria-labelledby="formTitle" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <?php print $content; ?>
      </div>
    </div>
  </div>
</div>

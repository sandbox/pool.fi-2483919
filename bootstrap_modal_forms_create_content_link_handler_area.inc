<?php

/**
 * @file
 * Add a create node link to views.
 * @todo: Link template.
 */
class bootstrap_modal_forms_create_content_link_handler_area extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();
    $options['content_type'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['content_type'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#default_value' => $this->options['content_type'],
      '#description' => t('The content type to create a link for.'),
      '#options' => node_type_get_names(),
    );
  }

  /**
   * Render the area
   */
  function render($empty = FALSE) {
    if ((!$empty || !empty($this->options['empty'])) && !empty($this->options['content_type']) && node_access('create', $this->options['content_type'])) {
      $id = drupal_html_id('add_' . $this->options['content_type']);
      $path = url('node/add/' . $this->options['content_type'] . '/modal');
      $link = '<a id="' . $id . '" href="' . $path  . '" class="use-ajax"><span class="btn btn-primary btn-lg">' . t('Create !type', array('!type' => node_type_get_name($this->options['content_type']))) . '</span></a>';
      // $link = l($text, $path, array('attributes' => array('class' => array('use-ajax'))));
      return $link;
    }
    return '';
  }
}

<?php

/**
 * @file
 * Views integration code.
 */
 
/**
 * Implements hook_views_data().
 */
function bootstrap_modal_forms_views_data() {
  $data['views']['bootstrap_modal_forms_create_content_link'] = array(
    'title' => t('Create content modal link'),
    'help' => t('Add a link to create content.'),
    'area' => array(
      'handler' => 'bootstrap_modal_forms_create_content_link_handler_area',
    ),
  );
  return $data;
}

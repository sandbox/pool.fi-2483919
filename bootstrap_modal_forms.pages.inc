<?php
/**
 * @file
 *   Page callbacks for the bootstrap_modal_forms module.
 */

/**
 * Form page callback.
 */
function bootstrap_modal_forms_form($form_id) {
  // Build the form.
  // @see drupal_get_form().
  $form_state = array();
  $includes = array();
  $args = func_get_args();
  $commands = array();

  // Remove $form_id from the arguments.
  array_shift($args);

  // Alter form id, arguments and required includes.
  $hooks = array(
    'bootstrap_modal_forms_arguments',
    'bootstrap_modal_forms_' . $form_id . '_arguments',
  );
  $base_form = $form_id;
  drupal_alter($hooks, $form_id, $args, $includes);

  if ($form_id) {
    $form_state['build_info']['args'] = $args;

    // Set a flag that this is a modal form in form_state.
    // @see bootstrap_modal_forms_form_alter().
    $form_state['#bootstrap_modal_forms'] = TRUE;

    // Include files needed by form.
    foreach ($includes as $include) {
      list($type, $module, $file_name) = $include;
      form_load_include($form_state, $type, $module, $file_name);
    }

    $form = drupal_build_form($form_id, $form_state);
    $commands[] = bootstrap_modal_forms_display(BOOTSTRAP_MODAL_FORMS_WRAPPER, $form);

    // Alter commands array prior to return.
    $hooks = array(
      'bootstrap_modal_forms_commands',
      'bootstrap_modal_forms_' . $form_id . '_commands',
    );
    if ($base_form != $form_id) {
      $hooks[] = 'bootstrap_modal_forms_' . $base_form . '_commands';
    }
    drupal_alter($hooks, $commands, $form, $form_state);
  }
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Implements hook_bootstrap_modal_FORM_ID_arguments_alter().
 *
 * Prepare arguments for building node add/edit forms.
 */
function bootstrap_modal_forms_bootstrap_modal_forms_node_form_arguments_alter(&$form_id, &$args, &$includes) {
  $node = array_shift($args);
  $content_type = _node_extract_type($node);
  $user = $GLOBALS['user'];
  // Node create forms have content type as argument.
  if (is_object($node) && isset($node->nid)) {
    $type_name = node_type_get_name($node);
    drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => $type_name, '@title' => $node->title)), PASS_THROUGH);
  }
  else {
    $node = (object) array(
      'uid' => $user->uid,
      'name' => (isset($user->name) ? $user->name : ''),
      'type' => $content_type,
      'language' => LANGUAGE_NONE,
    );
    $type_name = node_type_get_name($node);
    drupal_set_title(t('Create @node_type', array('@node_type' => $type_name)));
  }

  $types = node_type_get_types();

  // Node form callbacks are defined in node.pages.inc,
  // Add as include to form.
  $includes[] = array('inc', 'node', 'node.pages');

  // Change form callback for content type.
  $form_id = $content_type . '_node_form';
  $args = array($node);
}

/**
 * Implements hook_bootstrap_modal_FORM_ID_arguments_alter().
 *
 * Prepare arguments for building node add/edit forms.
 */
function bootstrap_modal_forms_bootstrap_modal_forms_node_delete_confirm_arguments_alter(&$form_id, &$args, &$includes) {
  // Node form callbacks are defined in node.pages.inc,
  // Add as include to form.
  $includes[] = array('inc', 'node', 'node.pages');
}

/**
 * Page callback for modal nodes.
 */
function bootstrap_modal_forms_entity_view_page($entity, $entity_type = 'node', $view_mode = 'full') {
  $entities = entity_view($entity_type, array($entity), $view_mode, NULL, NULL);
  if (!empty($entities[$entity_type])) {
    $output = reset($entities[$entity_type]);
    $variables = array(
      'title' => entity_label($entity_type, $entity),
      'content' => render($output),
    );
    $modal_content = theme('bootstrap_modal_forms_modal_content', $variables);
    $commands = array(bootstrap_modal_forms_display(BOOTSTRAP_MODAL_FORMS_WRAPPER, $modal_content));
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  return FALSE;
}

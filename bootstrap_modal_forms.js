/**
 * @file
 * Fetch form using AJAX.
 */
(function($) {
  Drupal.bootstrap_modal_forms = {};
  Drupal.bootstrap_modal_forms.display = function(ajax, response, status) {
    var $modal_container = $('#' + response.wrapperId);
    if ($modal_container.length) {
      // Modal exists, detach current behaviors and replace modal
      // content with new content from response.
      Drupal.detachBehaviors($('.bootstrap-modal', $modal_container));
      $new_content = $('.modal-content', $(response.modal));
      $('.modal-content', $modal_container).replaceWith($new_content);
      $modal = $('.bootstrap-modal', $modal_container);
      $modal.resize();
    }
    else {
      // Append modal to body.
      $('body').append(response.modal);
      $modal = $('#' + response.wrapperId + ' .bootstrap-modal');
    }
    // Attach behaviors and display modal.
    Drupal.attachBehaviors($modal);
    $modal.modal('show');
  }

  // Make command available to Drupal.
  Drupal.ajax.prototype.commands.bootstrap_modal_open = Drupal.bootstrap_modal_forms.display;
})(jQuery);
